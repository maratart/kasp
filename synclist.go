package main

import (
	"container/list"
	"fmt"
	"sync"
	"time"
)

type syncList struct {
	period int
	sync.Mutex
	list.List
}

func (m *syncList) plusOne() {
	m.pushElement(time.Now())
}

func (m *syncList) pushElement(t time.Time) {
	m.Lock()
	m.PushFront(t)
	m.Unlock()
}

func (m *syncList) removeElement(e *list.Element) {
	m.Lock()
	m.Remove(e)
	m.Unlock()
}

func (m *syncList) removeOld() {
	e := m.Back()
	if e == nil {
		return
	}
	ePrev := e.Prev()
	for {
		if e == nil {
			break
		}
		// Проходим список с конца, так как там самые старые запросы
		ePrev = e.Prev()
		if t, ok := e.Value.(time.Time); ok {
			// Убираем старые запросы
			if time.Now().Sub(t) > time.Second*time.Duration(m.period) {
				m.removeElement(e)
			} else {
				// Как только найден самый последный запрос, который попадает во временное окно, то прекращаем удаление, так как остальные запросы по умолчанию новее
				break
			}
		}
		// Переход к следующему элементу списка
		e = ePrev
	}
}

func (m *syncList) print() {
	m.Lock()
	for e := m.Back(); e != nil; e = e.Prev() {
		fmt.Println(e.Value)
	}
	m.Unlock()
}

func (m *syncList) count() int {
	m.Lock()
	defer m.Unlock()
	return m.Len()
}
