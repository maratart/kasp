package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"
)

var rList = syncList{}
var filenameState = "state.txt"
var serverAddr = ":8080"
var period = 60 // Окно в секундах

func main() {
	rList.period = period
	log.Println("load state")
	if err := loadState(&rList); err != nil {
		log.Println(err)
	}
	rList.print()
	go runWebServer()
	waitForCtrlC()
	log.Println("save state")
	rList.print()
	if err := saveState(&rList); err != nil {
		log.Println(err)
	}
}

func getCount(w http.ResponseWriter, r *http.Request) {
	rList.removeOld()
	w.Header().Add("Content-Type", "text/plain")
	fmt.Fprint(w, rList.count())
	fmt.Println("count:", rList.count())
	rList.plusOne()
}

func runWebServer() {
	http.HandleFunc("/", getCount)
	log.Fatal(http.ListenAndServe(serverAddr, nil))
}

func waitForCtrlC() {
	var endWaiter sync.WaitGroup
	endWaiter.Add(1)
	var signalChan chan os.Signal
	signalChan = make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		<-signalChan
		endWaiter.Done()
	}()
	endWaiter.Wait()
}

func saveState(l *syncList) error {
	var m = []time.Time{}
	for e := l.Front(); e != nil; e = e.Next() {
		if v, ok := e.Value.(time.Time); ok {
			m = append(m, v)
		}
	}
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(filenameState, b, 0644)
}

func loadState(l *syncList) error {
	b, err := ioutil.ReadFile(filenameState)
	if err != nil {
		return err
	}
	var m = []time.Time{}
	if err := json.Unmarshal(b, &m); err != nil {
		return err
	}
	for _, e := range m {
		l.pushElement(e)
	}
	return nil
}
